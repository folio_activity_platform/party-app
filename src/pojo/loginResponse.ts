export default class loginResponse {
    success: boolean;
    message: any;
    response: any;
    action: string; // '0'用户取消 '1'登录失败（包括密码账号没输，账号密码错误）‘2’ 成功

    constructor(success: boolean, message: any, response: any, action: string) {
        this.success = success;
        this.action = action;
        this.response = response;
        this.message = message;
    }
}
