import HttpClient from './HttpClients';
import { func } from 'prop-types';
export function login(param: object) {
  return HttpClient.post(`/bl-users/login`, param);
}
export function partyList(param: object) {
  return HttpClient.get(`/party/web/info`, param);
}
export function getImage(param: object) {
  return HttpClient.get(`/party/image`, param);
}
export function postImage(param: object) {
  return HttpClient.post(`/party/image`, param);
}
export function getPartyDetail(param: { barcode: string; id: string }) {
  return HttpClient.get(`/party/web/info/details/${param.barcode}/and/${param.id}`, null);
}
export function commitReserveData(params: any) {
  return HttpClient.post('/party/web/info/reader_reserve/' + params.partyId, params);
}
export function fetchPartyCategory(param: object) {
  return HttpClient.get(`/party/web/info/property`, param);
}
export function fetchReaderReserve(param: object) {
  return HttpClient.get(`/party/web/info/participation`, param);
}
export function cleanReserve(param: object) {
  return HttpClient.post(`/party/web/info/participation`, param);
}
//活动请假和签到 set_reader_checkIn_state
export function readerAttend(param: object) {
  return HttpClient.post('/party/web/info/participation_change', param);
}
export function fetchCalenderData(param: object) {
  return HttpClient.get('/party/web/info/calendar', param);
}
//读者提交活动评价
export function postAppraisal(param: any) {
  return HttpClient.post('/party/web/info/appraisal', param);
}

//获取读者评价
export function fetchAppraisal(param: any) {
  return HttpClient.get('/party/web/info/appraisal', param);
}

export function fetchAppraisalById(param: any) {
  return HttpClient.get('/party/web/info/appraisal/detail/'+param, null);
}

export function fatchReserveQueue(param: any) {
  return HttpClient.get('/party/web/info/reserve_queue', param);
}
export function postReserveQueue(param: any) {
  return HttpClient.post('/party/web/info/reserve_queue', param);
}
export function postUpdateReaderMessage(param:any){
  return HttpClient.post('/party/users', param);
}
export function getImageById(id: object) {
  return HttpClient.get(`/party/image/${id}`, null);
}