import axios from 'axios';
const commonUrl = process.env.REACT_APP_BASE_URL || 'http://localhost:9130';
const tenant = process.env.REACT_APP_TENANT || 'diku';
function toType(obj: any) {
    if (obj != null) {
        return obj.toString
            .call(obj)
            .match(/\s([a-zA-Z]+)/)[1]
            .toLowerCase();
    }
}
// 参数过滤函数
function filterNull(o: { [x: string]: any }) {
    Object.keys(o).map((key) => {
        if (o[key] === null) {
            delete o[key];
        }
        if (toType(o[key]) === 'string') {
            o[key] = o[key].trim();
        } else if (toType(o[key]) === 'object') {
            o[key] = filterNull(o[key]);
        } else if (toType(o[key]) === 'array') {
            o[key] = filterNull(o[key]);
        }
    });
    return o;
}
function generateUUID() {
    var d = new Date().getTime();

    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random() * 16) % 16 | 0;

        d = Math.floor(d / 16);

        return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16);
    });

    return uuid;
}
function apiAxios(method: any, url: string, params: any) {
    // if (params) {
    //     params = filterNull(params);
    // }
    return new Promise((resolve, reject) => {
        const token = sessionStorage.getItem('x-okapi-token');

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['X-Okapi-Tenant'] = tenant;
        if (token) {
            axios.defaults.headers.common['x-okapi-token'] = token;
        }
        url = commonUrl + url;
        axios({
            method,
            url,
            data: method === 'post' || method === 'put' ? params : null,
            params: method === 'get' || method === 'delete' || method === 'patch' ? params : null,
            withCredentials: false,
        })
            .then(
                (res) => {
                    if (res.status === 200 || res.status === 201) {
                        resolve(res);
                    } else {
                        reject('Axios返回状态不对，查看请求处理过程．．．．');
                    }
                },
                (err) => {
                    if (err['response']) {
                        const response = err.response;
                        const data = response.data;
                        if (!!data['errors']) {
                            const errors: any[] = data.errors;
                            const errObj = errors[0];
                            const q = errObj.message;
                            if (q.search('username') != -1) {
                                throw '请检查您输入的用户名称';
                            } else if (q.search('Password') != -1) {
                                throw '请检查您输入的登录密码';
                            } else if (q.search('active') != -1) {
                                throw '您的账号已经被锁定，解锁请联系管理员';
                            } else {
                                throw q;
                            }
                        }
                        throw err;
                    }
                }
            )
            .catch((err) => {
                reject(err);
            });
    });
}
export default {
    get: (url: string, params: any) => {
        return apiAxios('get', url, params);
    },
    post: (url: string, params: any) => {
        return apiAxios('post', url, params);
    },
    put: (url: string, params: any) => {
        return apiAxios('put', url, params);
    },
    delete: (url: string, params: any) => {
        return apiAxios('delete', url, params);
    },
    patch: (url: string, params: any) => {
        return apiAxios('patch', url, params);
    },
};
