import React, { Component } from 'react';
import { TabBar } from 'antd-mobile';
import { RouteComponentProps } from 'react-router-dom';
interface Props extends RouteComponentProps {
  history: any;
}
interface State {
  name?: string;
  enthusiasmLevel?: number;
  home: boolean;
  reserve: boolean;
  content: boolean;
}
class tabBar extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      home: true,
      reserve: false,
      content: false,
    };
  }
  componentDidMount() {
    const key = sessionStorage.getItem('barKey');
    let state: any = this.state;
    let objectKey: string[] = Object.keys(state);
    objectKey.forEach((a) => {
      if (key == a) {
        state[a] = true;
      } else {
        state[a] = false;
      }
    });

    this.setState({
      ...state,
    });
  }
  render() {
    const { history } = this.props;
    const { home, reserve, content } = this.state;

    return (
      <div style={{ position: 'fixed', width: '100%', bottom: '0px', left: '0px', zIndex: 112 }}>
        <TabBar unselectedTintColor="#949494" tintColor="#33A3F4" barTintColor="white" hidden={false}>
          <TabBar.Item
            title="首页"
            key="Home"
            icon={
              <div
                style={{
                  width: '22px',
                  height: '22px',
                  background: home
                    ? 'url(/images/icons/sy-x.png) center center /  21px 21px no-repeat'
                    : 'url(/images/icons/sy-w.png) center center /  21px 21px no-repeat',
                }}
              />
            }
            onPress={() => {
              history.push('/');
              sessionStorage.setItem('barKey', 'home');
            }}
            data-seed="logId"></TabBar.Item>
          <TabBar.Item
            title="活动列表"
            key="Reserve"
            icon={
              <div
                style={{
                  width: '22px',
                  height: '22px',
                  background: reserve
                    ? 'url(/images/icons/lb-x.png) center center /  21px 21px no-repeat'
                    : 'url(/images/icons/lb-w.png) center center /  21px 21px no-repeat',
                }}
              />
            }
            onPress={() => {
              history.push('/list');
              sessionStorage.setItem('barKey', 'reserve');
            }}
            data-seed="logId"></TabBar.Item>
          <TabBar.Item
            title="用户中心"
            key="Content"
            icon={
              <div
                style={{
                  width: '22px',
                  height: '22px',
                  background: content
                    ? 'url(/images/icons/gr-x.png) center center /  21px 21px no-repeat'
                    : 'url(/images/icons/gr-w.png) center center /  21px 21px no-repeat',
                }}
              />
            }
            onPress={() => {
              history.push('/user_content');
              sessionStorage.setItem('barKey', 'content');
            }}
            data-seed="logId"></TabBar.Item>
        </TabBar>
      </div>
    );
  }
}

export default tabBar;
