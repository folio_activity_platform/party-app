import React,{ useEffect,useState} from 'react';
import {fetchAppraisalById} from '../../api/index'
import {setImageById} from '../../util/imageUtil'
import {Modal,Card, WingBlank, WhiteSpace,Grid } from 'antd-mobile'


function loadImage(idList:any[]) {
    return new Promise( async(resolve,reason)=>{
          let list:any = []
          for(let i = 0 ; i<idList.length ; i++){
              let id = idList[i]
             let result =  await setImageById(id);
             list.push(result)
          }
          resolve(list)
    })
}

const Detail=(props:any)=>{
    
    const{match ,history} = props
    const [aid,setAid] = useState("")
    const[partyImage,setPartyImage] = useState("")
    const[imageList,setImageList] = useState([])
    const[data,setData] = useState({createDate:'',appraisal:'',partyName:'',partyStartDate:'',partyId:''})
    const[userData,setUserData]=useState({name:''});
    const[starsNode ,setStartsNode]=useState([ ])
    useEffect(()=>{
        const{id} = match.params
        const param  = id;
        setAid(aid);
        fetchAppraisalById(param).then((res:any) =>{
           if(!!res.data){
              let adata=res.data;
              setData(res.data)
              setUserData(res.data.readerReserveGroup[0]);
            if(adata.partyImageId){
                setImageById(adata.partyImageId).then((partyImage:any)=>{
                    setPartyImage(partyImage)
                })
            }
              loadImage(adata.imageIdList).then((imgList:any)=>{
                  setImageList(imgList);
              }).catch(e=>{
                  console.debug(e)
              })
              const score = parseInt(adata.score)
              const starNode: any = []
               for (let i = 0; i < score; i++) {
                  starNode.push(
                     <label   style={{ zoom: 0.4, float: "none",backgroundImage:'url(/images/icons/star-x.png)' }}></label>
                  )
              }
              setStartsNode(starNode)
           }else{
        
           }
        }).catch(e=>{
            console.debug(e)
        })
    },[aid])
    return <div>
    <WingBlank size="lg">
    <WhiteSpace size="lg" />
    <Card>
      <Card.Header
        title={<div><div style={{fontSize:'xx-small' }}>{userData.name}</div><div style={{fontSize:'xx-small',color:'#777'}}>{data.createDate}</div></div>}
        thumb="/images/user-img.png"
        thumbStyle={{width:36,height:36}}
        extra={<fieldset className="starability-slot" style={{ width: '100%' }}>
            {
                  starsNode.map((item: any) => {
                    return item
                  })
            }
        </fieldset>}
      />
      <Card.Body>
        <div>
           <p>&nbsp;&nbsp;&nbsp;&nbsp;{data.appraisal}</p>
          <div style={{marginTop:15}}>
           <Grid data={imageList}
                columnNum={3}
                
                renderItem={(dataItem:any) => (
                 <div >
                    <img src={dataItem } style={{ width: '75px', height: '75px' }} alt="" />
                   </div>
      )}
    />
    </div>
        </div>
      </Card.Body>
      <Card.Footer content={<div style={{   height: '95px',padding:10 }}  >
          <div style={{   height: '100%', border:'1px solid #bbb',borderRadius:5 }} >
          <div style={{padding:5}}  >
           <img src={partyImage} style={{width:130,height:63,float:'left'}}></img>
          </div>
          <div style={{float:'right',marginRight:30}}>
          <ul onClick={()=>{
              history.push('/party_details/'+data.partyId)
          }}>
              <li style={{width:110}} ><div style={{
                  whiteSpace: 'nowrap',
                  overflow: 'hidden',
                  textOverflow: 'ellipsis'
                }}>{data.partyName}</div></li>
              <li style={{fontSize:'xx-small'}}>{data.partyStartDate}</li></ul> 
          </div>
          </div>
          </div>}   />
    </Card>
    <WhiteSpace size="lg" />
  </WingBlank>
    </div>
} 


export default Detail;
