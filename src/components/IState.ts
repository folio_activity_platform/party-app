export default interface IState {
    // eslint-disable-next-line @typescript-eslint/ban-types
    form: object;
    offset: number;
    limit: number;
    query: string;
    isLogin?: boolean;
    user?: any;
    readerActivityList?: React.ReactElement<any>[];
}
