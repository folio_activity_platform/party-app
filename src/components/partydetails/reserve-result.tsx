import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router-dom';
interface Props extends RouteComponentProps {
    match: any;
}
interface State {
    showFlag: boolean;
    party: any;
    id: string;
    user: any;
}

class ReserveResult extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            showFlag: false,
            party: {},
            id: '',
            user: {},
        };
    }
    componentDidMount() {
        const {
            match: {
                params: { id },
            },
            history: { push },
        } = this.props;
        let userJsonStr = sessionStorage.getItem('user') || '';
        if (!userJsonStr) {
            push('/login');
        }
        if (!!id && !!userJsonStr) {
            let partyJSONString = sessionStorage.getItem(id) || '';

            if (!!partyJSONString) {
                const party = JSON.parse(partyJSONString);
                let user = JSON.parse(userJsonStr);
                let showFlag = false;
                if (party['category'] === 2) {
                    showFlag = true;
                }
                this.setState({
                    party,
                    id,
                    user,
                    showFlag,
                });
                return;
            }
            push('/');
        }

        push('/');
    }
    render() {
        return (
            <div>
                <div className="c-wrapper">
                    <div className="c-check-icon">
                        <div className="c-img success">
                            <img src="/images/icons/success.png" />
                        </div>
                        <div className="c-img error">
                            <img src="/images/icons/err.png" />
                        </div>
                    </div>
                    <div className="c-text">
                        {this.state.showFlag ? (
                            <h4>活动报名需要审核，请耐心等待审核</h4>
                        ) : (
                            <span>
                                <h4>报名成功</h4>
                                <p> 可到个人中心查询</p>
                            </span>
                        )}
                    </div>
                    <div className="c-btn-box">
                        <a
                            className="c-btn default-a"
                            onClick={() => {
                                this.props.history.push('/user_content');
                            }}>
                            个人中心
                        </a>
                        <a
                            className="c-btn default"
                            onClick={() => {
                                this.props.history.push('/list');
                            }}>
                            返回列表页
                        </a>
                        <a
                            className="c-btn default"
                            onClick={() => {
                                this.props.history.push('/');
                            }}>
                            返回首页
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default ReserveResult;
