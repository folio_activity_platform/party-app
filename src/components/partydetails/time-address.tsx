import React, { Component } from 'react';
import copy from 'copy-to-clipboard';
import DateUtil from '../../util/DateUtil';
import { Toast } from 'antd-mobile';
interface IProps {
    party: any;
    data: any;
}
class index extends Component<IProps, {}> {
    constructor(props: IProps) {
        super(props);
    }

    render() {
        return (
            <div className="activity-top-time">
                <div className="act-t">
                    <div className="activity-hint clearfix">
                        <span style={{ fontSize: '18px' }}>{this.props.party.partyName}</span>
                    </div>
                </div>
                <div className="act-t">
                    <div className="activity-hint clearfix">
                        <span>活动时间：</span>
                        <span className={'font'}>
                            {DateUtil.mergeFormat(this.props.party.partyStartDate, this.props.party.partyEndDate)}
                        </span>
                    </div>
                </div>
                <div className="act-t" style={{ display: !!this.props.party.reserveStartDate ? 'block' : 'none' }}>
                    <div className="activity-hint clearfix">
                        <span>报名时间：</span>
                        <span className={'font'}>
                            {DateUtil.mergeFormat(this.props.party.reserveStartDate, this.props.party.reserveEndDate)}
                        </span>
                    </div>
                </div>
                <div className="act-t" style={{ display: !!this.props.party.attendStartDate ? 'block' : 'none' }}>
                    <div className="activity-hint clearfix">
                        <span>签到时间：</span>
                        <span className={'font'}>
                            {DateUtil.mergeFormat(this.props.party.attendStartDate, this.props.party.attendEndDate)}
                        </span>
                    </div>
                </div>
                <div className="act-b">
                    <div className="act-tishi">
                        <span>活动地点：</span>
                        <span className={'font'}> {this.props.party.venue} </span>
                    </div>
                    <div className="act-tishi">
                        <span>剩余名额：</span>
                        <span className={'font'} style={{ color: 'red' }}>
                            {parseInt(this.props.party.quota) - parseInt(this.props.data.reserveAmount)}
                        </span>
                        <span>/{this.props.party.quota}</span>
                    </div>
                    <div className="act-tishi" style={{ display: !!this.props.party.liveUrl ? 'block' : 'none' }}>
                        <div className={'font video-link-light'}>视频链接：</div>
                        <div className={'font video-link-right'}>{this.props.party.liveUrl}</div>
                        <div>
                            <span
                                className="video-link-right-span-button"
                                onClick={() => {
                                    if (copy(this.props.party.liveUrl)) {
                                        Toast.success('复制成功');
                                    } else {
                                        Toast.fail('复制失败');
                                    }
                                }}>
                                点我复制
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default index;
