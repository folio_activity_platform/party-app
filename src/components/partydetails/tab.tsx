import React from 'react';
import { Tabs, WhiteSpace, Badge } from 'antd-mobile';
import AppraisalList from './appraisal-list'
const baseStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  marginBottom: '50px',
  backgroundColor: '#fff',
};
interface Props {
  depict?: any;
  venueImage?: any;
  partyId: string
  ,history:any
}
function depictInit(depict: any) {
  if (!!depict) {
    return (
      <div className="act-tabCon1" style={{ display: 'block' }}>
        <p
          style={{ display: 'block' }}
          dangerouslySetInnerHTML={{
            __html: decodeURI(depict),
          }}></p>
      </div>
    );
  }
  return <div></div>;
}
function venueImageInit(venueImage: any) {
  if (!!venueImage) {
    return (
      <div className="act-tabCon1" style={{ display: 'block' }}>
        <img src={venueImage}></img>
      </div>
    );
  } else {
    return <div></div>;
  }
}
const TabExample = (Props: Props) => {
  const tabs = [{ title: <Badge>详 情</Badge> }, { title: <Badge>活 动 评 价</Badge> }];
  if (!!Props.venueImage) {
    tabs.push({ title: <Badge>场 地 指 引</Badge> });
  }
  console.log(Props)
  return (
    <div>
      <Tabs
        tabs={tabs}
        initialPage={0}
        tabBarActiveTextColor="red"
        tabBarUnderlineStyle={{ border: '1px #108ee9 solid' }}>
        <div style={baseStyle}>{depictInit(Props.depict)}</div>
        <div style={baseStyle}>{AppraisalList({ partyId: Props.partyId,history:Props.history })}</div>
        <div style={baseStyle}>{venueImageInit(Props.venueImage)}</div>
      </Tabs>

      <WhiteSpace />
    </div>
  );
};
export default TabExample;
