import React, {  useEffect,useState } from 'react';
import { Modal, Toast } from 'antd-mobile';
import { Formik } from 'formik';
import * as yup from 'yup'
interface Props {
    supplement:boolean,
    changeSupplement:Function
    modifySupplement:Function
  }
 
const Supplement = (Props:Props)=>{
 
    const [open,setOpen] = useState(Props.supplement||false)

    useEffect(() => {
        setOpen(Props.supplement)
    }, [Props.supplement]);

    function formSubmit ({...forms} )   {
        console.log(forms)
        return true;
    }

    return  <Modal
    visible={open}
    transparent
    maskClosable={false}
    onClose={()=>{
      setOpen(false)
    }}
    title="填写联系方式"
 
    >
    <div className="am-modal-propmt-content">
    <Formik
       validationSchema={yup.object().shape({
          mobile:yup.string().
          trim().required("手机号码不能为空")
          .matches(/0?(13|14|15|18|17)[0-9]{9}/
          ,{message:'请检查您输入的号码格式',excludeEmptyString:true}),
          email:yup.string().
          matches(/\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/
          ,{message:'请检查您输入的邮箱格式',excludeEmptyString:true})
       })}
       initialValues={{ mobile: '', email: '' }}
                        onSubmit={(values, actions) => {
                            
                            Props.modifySupplement(values)
                        }}
                        render={(p) => (
                            <form onSubmit={p.handleSubmit} >
                                  <div className="am-modal-input-container">
                                  {p.touched.mobile && p.errors.mobile && <div style={{color:'red',fontSize:'x-small'}}>{p.errors.mobile}</div>}
                                  
                              <div className="am-modal-input">
                                    <input
                                     
                                        type="tel"
                                        onChange={p.handleChange}
                                        onBlur={p.handleBlur}
                                        name="mobile"
                                        placeholder="请输入手机号码"
                                    
                                    />
                                  
                                </div>
                                {p.touched.email && p.errors.email && <div style={{color:'red',fontSize:'x-small'}}>{p.errors.email}</div>}  
                                <div className="am-modal-input">
                                    <input
                                        type="email"
                                        onChange={p.handleChange}
                                        onBlur={p.handleBlur}
                                        name="email"
                                        placeholder="请输入电子邮箱号(选填)"
                                      
                                    />
                                </div>
                                </div>
                                <div className="am-modal-footer">
                                    <div className="am-modal-button-group-h am-modal-button-group-normal" role="group">
                                        <input className="am-modal-button" role="button" type="button" value='取消' style={{backgroundColor:'#ddd'}} onClick={()=>{Props.changeSupplement()}}/> 
                                        <input className="am-modal-button" role="button" type='submit' value='确认' /> 
                                        </div>
                                    </div>
                               
                            </form>
             )}></Formik>
         </div>
  </Modal>

}
export default Supplement;