import { RouteComponentProps } from 'react-router-dom';
export default interface IProps extends RouteComponentProps {
    className?: string;
}
