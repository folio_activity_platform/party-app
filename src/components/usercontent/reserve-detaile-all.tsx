import React, { Component, createRef } from 'react';
import ReserveList from '../list/reserve-list';
import { loginNext } from '../../util/login';

import IProps from '../IProps';
interface Props extends IProps {
    match: any;
}
interface IState {
    barcode: string;
    query: string;
    status: boolean;
}
class MyComponent extends Component<Props, IState> {
    private myList = createRef<ReserveList>();
    constructor(props: Props) {
        super(props);
        this.state = {
            barcode: '',
            query: '',
            status: false,
        };
    }
    componentDidMount() {
        let then = this;
        let barcode = sessionStorage.getItem('barcode');

        const {
            match: {
                params: { id },
            },
        } = this.props;
        if (!!barcode) {
            let query = this.selectParams() || '';
            sessionStorage.setItem('query', this.selectParams() || '');
            this.setState({ query, status: true }, () => {
                then.myList.current!.loadData();
            });
        } else {
            loginNext(then, function() {
                sessionStorage.setItem('query', then.selectParams() || '');
                then.myList.current!.attend(id, false);
            });
        }
    }

    selectParams = () => {
        let barcode = sessionStorage.getItem('barcode');
        if (barcode != null) {
            let query = ` isDel = 0 and readerReserveGroup == *"barcode"*: *"${barcode}"* `;
            this.setState({ barcode: barcode, query });
            return query;
        } else {
            return null;
        }
    };



    render() {
        return (
            <div style={{ backgroundColor: '#f5f5f9' }}>
                <div className="my-nav">
                    <span
                        id="selectId01"
                        onClick={() => {
                            this.props.history.push('/reserve_all_today');
                        }}>
                        今日活动
                    </span>
                    <span
                        id="selectId02"
                        onClick={() => {
                            this.props.history.push('/reserve_all_wait');
                        }}>
                        待参加
                    </span>
                    <span
                        id="selectId03"
                        className={'active'}
                        onClick={() => {
                            this.props.history.push('/reserve_all_detail/all');
                        }}>
                        全部
                    </span>
                </div>

                <div className="myp-drop">
                    <ReserveList
                        ref={this.myList}
                        offset={0}
                        limit={5}
                        query={this.state.query}
                        maxSize={5}
                        {...this.props}></ReserveList>
                </div>
                <div
                    className="list-goback"
                    onClick={() => {
                        this.props.history.push('/');
                        sessionStorage.setItem('barKey', 'home');
                    }}>
                    <span>返回</span>
                </div>
            </div>
        );
    }
}

export default MyComponent;
