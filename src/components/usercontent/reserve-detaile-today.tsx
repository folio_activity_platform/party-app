import React, { Component, createRef } from 'react';
import ReserveList from '../list/reserve-list';
import { loginNext } from '../../util/login';
import DateUtil from '../../util/DateUtil';
import IProps from '../IProps';
interface Props extends IProps {
    match: any;
}
interface IState {
    barcode: string;
    query: string;
}
class MyComponent extends Component<Props, IState> {
    private myList = createRef<ReserveList>();
    constructor(props: Props) {
        super(props);
        this.state = {
            barcode: '',
            query: '',
        };
    }
    componentDidMount() {
        let then = this;
        let barcode = sessionStorage.getItem('barcode');
        if (!!barcode) {
            // let query = this.selectParams() || '';
            // this.setState({ query });
            let query = this.selectParams() || '';
            this.setState({ query }, () => {
                then.myList.current!.loadData();
            });
        } else {
            loginNext(then, function() {
                then.props.history.push('/reserve_all_today');
            });
        }
    }

    selectParams = () => {
        let barcode = sessionStorage.getItem('barcode');
        if (barcode != null) {
            let query = ` isDel = 0 and readerReserveGroup == *"barcode"*: *"${barcode}"* `;
            let str = DateUtil.freeObjFormat(new Date(), 'YYYY-MM-DD');

            query += `and partyEndDate >= ${str} 00:00:00  and partyStartDate <= ${str} 23:59:59  `;

            this.setState({ barcode: barcode, query });
            return query;
        } else {
            return null;
        }
    };
    render() {
        return (
            <div style={{backgroundColor: 'rgb(245, 245, 249)'}}>
                <div className="my-nav">
                    <span
                        id="selectId01"
                        className={'active'}
                        onClick={() => {
                            this.props.history.push('/reserve_all_today');
                        }}>
                        今日活动
                    </span>
                    <span
                        id="selectId02"
                        onClick={() => {
                            this.props.history.push('/reserve_all_wait');
                        }}>
                        待参加
                    </span>
                    <span
                        id="selectId03"
                        onClick={() => {
                            this.props.history.push('/reserve_all_detail/all');
                        }}>
                        全部
                    </span>
                </div>

                <div className="myp-drop">
                    <ReserveList
                        ref={this.myList}
                        offset={0}
                        limit={5}
                        query={this.state.query}
                        maxSize={5}
                        {...this.props}></ReserveList>
                </div>
                <div
                    className="list-goback"
                    onClick={() => {
                        this.props.history.push('/');
                        sessionStorage.setItem('barKey', 'home');
                    }}>
                    <span>返回</span>
                </div>
            </div>
        );
    }
}

export default MyComponent;
