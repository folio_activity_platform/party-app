import React, { Component } from 'react';

interface Props {
  type: string;
}
interface State {
  data: any[];
}
class header extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentDidMount() {
    const selectId01 = document.createElement('span');
    selectId01.setAttribute('id', 'selectId01');
    selectId01.innerText = '今日活动';
    const selectId02 = document.createElement('span');
    selectId02.setAttribute('id', 'selectId02');
    selectId02.innerText = '待参加';
    const selectId03 = document.createElement('span');
    selectId03.setAttribute('id', 'selectId03');
    selectId03.innerText = '全部';
    const obj = [selectId01, selectId02, selectId03];

    this.setState({
      data: obj,
    });
  }
  render() {
    return (
      <div className="my-nav">
        {this.state.data.map((a: any) => {
          return a;
        })}
      </div>
    );
  }
}
export default header;
