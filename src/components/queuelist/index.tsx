import React, { Component, useEffect, useState } from 'react';
import { fatchReserveQueue, postReserveQueue } from '../../api/index';
import { Toast, Modal, Card, Flex, Button, Checkbox, ListView } from 'antd-mobile';
import { setImage } from '../../util/imageUtil';
const alert = Modal.alert;

function MyBody(props: any) {
    return <div className="myp-drop">{props.children}</div>;
}
function SmallBody(props: any) {
    return <div style={{ margin: '8px' }}> {props.children} </div>;
}


function separator(sectionID: any, rowID: any) {
    return <div
        key={`${sectionID}-${rowID}`}
        style={{
            backgroundColor: '#F5F5F9',
            height: 8,
            borderTop: '1px solid #ECECED',
            borderBottom: '1px solid #ECECED',
        }}
    />
}
const List = (props: any) => {
    const [dataSource, setDataSource] = useState(new ListView.DataSource({
        rowHasChanged: (row1: any, row2: any) => row1 !== row2,
    }))

    const [list, setList] = useState([])
    const [barcode, setBarcode] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [hasMore, setHasMore] = useState(false)
    useEffect(() => {
        if (!barcode) {
            const barcode = sessionStorage.getItem("barcode");
            setBarcode(barcode || '')
            loadData()
        }
    });

    function loadData() {
        const code = sessionStorage.getItem("barcode");
        fatchReserveQueue({ query: code }).then((response: any) => {
            const { data: { reserveGroup, totalRecords } } = response
            const dataList = list.concat(reserveGroup);
            if (dataList.length <= response.data.totalRecords) {
                setHasMore(true);
            } else {
                setHasMore(false);
            }
            setList(dataList);
            setDataSource(dataSource.cloneWithRows(dataList))
            setIsLoading(false);
        })
    }

    function onEndReached() {

    }
    const row = (rowData: any, sectionID: string | number, rowID: string | number, highlightRow?: boolean) => {
        setImage(rowData, 1).then((value: any) => {
            let dom = document.getElementById(rowData.imageId);
            if (dom) {
                dom.setAttribute('src', value);
            }
        });
        return (
            <Card key={`${sectionID}-${rowID}`}>
                <Flex>
                    <Flex.Item style={{ margin: 8 }}><img id={rowData.imageId} style={{ borderRadius: 8 }} src='' alt="加载中..."></img></Flex.Item>
                    <Flex.Item >
                        <div style={{ top: 8, position: 'absolute' }}>{rowData.partyName}</div>
                        <div style={{
                            fontSize: 7,
                            position: 'absolute',
                            top: 30,
                            color: '#bbb'
                        }}>{rowData.reserveDate}</div>
                        <div style={{
                            fontSize: 7,
                            position: 'absolute',
                            top: 50,
                            color: '#bbb'
                        }}>排队中</div>
                        <div style={{

                            textAlign: "center",
                            marginTop: 70,
                            color: '#389fc3'
                        }} onClick={() => {
                            alert('提示', '你确定取消排队???', [
                                { text: '关闭', onPress: () => console.log('cancel') },
                                {
                                    text: '确定',
                                    onPress: () =>
                                        new Promise((resolve) => {
                                            const data = rowData;
                                            resolve();
                                            Toast.loading('取消活动排队中...')
                                            postReserveQueue(data).then((res: any) => {
                                                Toast.hide()
                                                Toast.success('取消成功!', 2, () => {
                                                    window.location.reload()
                                                })
                                            }).catch((e) => {
                                                console.log(e)
                                                Toast.hide()
                                                Toast.fail('取消排队失败!')
                                            })

                                        }),
                                },
                            ])
                        }}>
                            <div style={{ border: '1px solid', width: '80%', borderRadius: 5 }}> 取消排队 </div>
                        </div>
                    </Flex.Item>

                </Flex>

            </Card>
        );
    }
    return <ListView
        style={{ height: window.innerHeight }}
        dataSource={dataSource}
        className="am-list sticky-list"
        renderFooter={() => (
            <div style={{ padding: 30, textAlign: 'center', marginTop: 40 }}>
                { isLoading ? '加载中...' : '加载完毕'}
            </div>
        )}
        renderRow={row}
        initialListSize={10}
        pageSize={10}
        onEndReachedThreshold={50}
        renderSeparator={separator}
        renderBodyComponent={() => <MyBody />}
        renderSectionBodyWrapper={() => <SmallBody />}
        onEndReached={onEndReached}
    />
}
export default List;