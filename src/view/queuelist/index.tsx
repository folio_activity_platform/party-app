import React, { Component, useEffect, useState } from 'react';
import List from '../../components/queuelist/index'
const QueueList = (props: any) => {
 
    return <div>
        <List />
        <div
            className="list-goback"
            onClick={() => {
                props.history.push('/');
                sessionStorage.setItem('barKey', 'home');
            }}>
            <span>返回</span>
        </div>
    </div>

}
export default QueueList;