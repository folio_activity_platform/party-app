import React from 'react';
import List from '../../components/appraisal/list'
import {WhiteSpace} from 'antd-mobile'
interface indexProps {
    history:any
}

interface indexState {
    
}

class index extends React.Component<indexProps, indexState> {
    state = {}

    render() {
        return (
           <div>
               <WhiteSpace size="xl" />
               <List offset={0} limit={5} query={''} maxSize={10} {...this.props}/>
          <div
            className="list-goback"
            onClick={() => {
                this.props.history.push('/');
                sessionStorage.setItem('barKey', 'home');
            }}>
            <span>返回</span>
        </div>
           </div>
        );
    }
}


export default index;
