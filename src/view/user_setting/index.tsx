import React, { Component } from 'react';
import IProps from '../../components/IProps';

interface IState {
    user: any;
    personal: any;
}
class index extends Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            user: {},
            personal: {},
        };
    }

    componentWillMount() {
        const user_json = sessionStorage.getItem('user');

        if (user_json) {
            const userObject = JSON.parse(user_json);
            this.setState({ user: userObject, personal: userObject.personal }, () => {});
        }
    }

    render() {
        console.log(this.state);
        return (
            <div className="graybg">
                <div className="u-info-title">个人信息</div>
                <div className="myp-wrap">
                    <a>
                        <span>头像</span>
                        <div className="u-info-right">
                            <img className="s-u-info-img" src="/images/user-img.png" alt="" />
                        </div>
                        {/* <i className="iconspan jiantou"></i> */}
                    </a>
                    <a>
                        <span>用户名</span>
                        <div className="u-info-right">
                            <span>{this.state.user['username']}</span>
                        </div>
                        {/* <i className="iconspan jiantou"></i> */}
                    </a>

                    {/* <label> */}
                    {/* <span>性别</span>
            <div className="u-info-right">
              <input
                type="text"
                id="sexStyle"
                className="sex-input"
                value="男"
                name=""
                value=""
                placeholder=""
                readonly="readonly"
                nselectable="on"
                onfocus="this.blur()"
              />
            </div> */}
                    {/* <i className="iconspan jiantou"></i> */}
                    {/* </label> */}

                    <a>
                        <span>读者证号</span>
                        <div className="u-info-right">
                            <span>{this.state.user.barcode}</span>
                        </div>
                        {/* <i className="iconspan jiantou"></i> */}
                    </a>
                    <a>
                        <span>手机号</span>
                        <div className="u-info-right">
                            <span>{this.state.personal['mobilePhone']}</span>
                        </div>
                        {/* <i className="iconspan jiantou"></i>{this.state.user.personal.mobilePhone} */}
                    </a>
                    <a>
                        <span>电子邮箱号</span>
                        <div className="u-info-right">
                            <span>{this.state.personal['email']}</span>
                        </div>
                        {/* <i className="iconspan jiantou"></i> */}
                    </a>
                </div>

                {/* <div className="u-info-title">
			账号与安全
		</div> */}

                {/* <div className="myp-wrap">
            <a href="changephone.html">
                <span>更换手机号</span>
                <div className="u-info-right">
                    <span>13555687412</span>
                </div> */}
                {/* <i className="iconspan jiantou"></i> */}
                {/* </a> */}
                {/* <a href="changepassword.html">
                <span>修改密码</span> */}

                {/* <i className="iconspan jiantou"></i> */}
                {/* </a> */}
                {/* <a >
                <span>解除微信绑定</span> */}

                {/* <i className="iconspan jiantou"></i> */}
                {/* </a> */}
                {/* </div> */}
            </div>
        );
    }
}

export default index;
