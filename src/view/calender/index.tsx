import React, { Component } from 'react';
import Calendar from 'react-calendar';
import { fetchCalenderData } from '../../api/index';
import List from '../../components/list';
import moment from 'moment';
import DateUtil from '../../util/DateUtil';
import IProps from '../../components/IProps';
interface IState {
    calendarGroup: any[];
    query: string;
    day: string;
}
class index extends Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            calendarGroup: [],
            query: '',
            day: DateUtil.freeObjFormat(new Date(), 'D'),
        };
    }

    componentWillMount() {
        this.getDayDataList(new Date());
        this.getMonthDataList(new Date());
    }

    componentDidMount() {}

    getMonthDataList = (date: Date) => {
        fetchCalenderData({ query: DateUtil.freeObjFormat(date, 'YYYY-MM-DD') }).then((response: any) => {
            let data = response.data;
            let calendarGroup = data.calendarGroup;
            this.setState({
                calendarGroup,
            });
        });
    };
    getDayDataList = (date: Date) => {
        this.changeButton(date);
    };
    changeButton = (date: any) => {
        const startDate = moment(date)
            .startOf('day')
            .format('YYYY-MM-DD HH:mm:ss');
        const endDate = moment(date)
            .endOf('day')
            .format('YYYY-MM-DD HH:mm:ss');
        let query = `isDel = 0 and approval = 1  and status = 1 and partyStartDate >=${startDate} and partyStartDate <= ${endDate} `;

        this.setState({
            query,
        });
    };

    render() {
        return (
            <div>
                <Calendar
                    onChange={this.changeButton}
                    value={new Date()}
                    locale="cn"
                    defaultView={'month'}
                    className="myCalender"
                    showNeighboringMonth={false}
                    onActiveStartDateChange={(value: any) => {
                        this.getMonthDataList(value.activeStartDate);
                        this.getDayDataList(value.activeStartDate);
                    }}
                    tileContent={({ date, view }) => {
                        let day = DateUtil.freeObjFormat(date, 'D');
                        let result: any = <div></div>;
                        this.state.calendarGroup.map((a: any) => {
                            console.log(typeof day);
                            if (a.day === day && parseInt(day) >= parseInt(this.state.day)) {
                                result = (
                                    <div className={'div_calendar_day_text'}>
                                        <span>{a.amount}</span>
                                    </div>
                                );
                            } else if (a.day === day && parseInt(day) < parseInt(this.state.day)) {
                                result = (
                                    <div className={'div_calendar_day_text_old'}>
                                        <span>{a.amount}</span>
                                    </div>
                                );
                            }
                        });
                        return result;
                    }}
                />
                <div style={{ margin: '35px 0px' }}>
                    <List offset={0} limit={5} query={this.state.query} maxSize={5} {...this.props} />
                </div>
            </div>
        );
    }
}

export default index;
