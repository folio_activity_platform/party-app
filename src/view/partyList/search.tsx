import React, { Component, createRef } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Icon, Modal, Toast, SearchBar, Button } from 'antd-mobile';
import List from '../../components/list';
import DateUtil from '../../util/DateUtil';
interface IProps extends RouteComponentProps {
    className?: string;
}
interface IState {
    query: string;
}
class search extends Component<IProps, IState> {
    private myList = createRef<List>();
    constructor(props: IProps) {
        super(props);
        this.state = {
            query: '',
        };
    }
    search = (values: any) => {
        let query = this.selectParams(values) || '';
        this.setState({
            query,
        });
    };

    selectParams = (partyName: string) => {
        let query = '(isDel = 0 and approval = 1  and status = 1 and isCalendar = 0 %{query} )';
        let queryParam = '';

        if (!!partyName) {
            queryParam += `and partyName == *${partyName}*`;
        }
        return DateUtil.paramReplace(query, queryParam);
    };
    render() {
        return (
            <div>
                <div className="at-top">
                    <div className="at-topcontent ">
                        <SearchBar
                            placeholder="请输入你需要查找的活动名称"
                            maxLength={20}
                            onSubmit={(values) => {
                                this.search(values);
                            }}
                            onClear={(values) => {
                                this.search('');
                            }}
                        />
                    </div>
                </div>
                <div style={{ marginTop: '45px' }}>
                    <List ref={this.myList} offset={0} limit={5} query={this.state.query} maxSize={0} {...this.props} />
                </div>
                <div
                    className="list-goback"
                    onClick={() => {
                        this.props.history.push('/');
                        sessionStorage.setItem('barKey', 'home');
                    }}>
                    <span>返回</span>
                </div>
            </div>
        );
    }
}

export default search;
