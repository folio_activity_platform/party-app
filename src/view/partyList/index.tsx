import React, { Component, createRef } from 'react';
import IProps from '../../components/IProps';
import { Icon, Modal, Toast } from 'antd-mobile';
import ToBar from '../../components/tabBar';
import List from '../../components/list';
import { ListView, List as selectList, Picker, Flex, Button } from 'antd-mobile';
import { fetchPartyCategory } from '../../api/index';
import DateUtil from '../../util/DateUtil';
const prompt = Modal.prompt;
interface State {
    sortShow: boolean;
    typeShow: boolean;
    offset: number | null;
    limit: number | null;
    query: string;
    selectList?: React.ReactElement<any>[];
    categoryList: React.ReactElement<any>[];
    activityType: string;
    activityTime: string;
    partyName: string;
    sValue: any;
    seasons: any[];
    selectButtonClassName: string;
}

class index extends Component<IProps, State> {
    private myList = createRef<List>();
    constructor(props: IProps) {
        super(props);
        this.state = {
            sortShow: false,
            typeShow: false,
            offset: null,
            limit: null,
            query: 'isDel = 0 and approval = 1  and status = 1 sortby metadata.createdDate/sort.descending ',
            selectList: [],
            categoryList: [],
            activityType: '',
            activityTime: 'allTime',
            partyName: '',
            sValue: '',
            seasons: [],
            selectButtonClassName: 'select-button',
        };
    }

    componentDidMount() {
        let param = {
            query: '(isDel == 0 and parentId <> 0 )',
        };
        fetchPartyCategory(param).then((response: any) => {
            let list = response.data.propertyGroup;

            let type = {
                label: '所有分类',
                value: 'allType',
            };

            let itemList: any[] = [type];

            if (list) {
                list.forEach((item: any) => {
                    itemList.push({
                        label: item.name,
                        value: item.id,
                    });
                });
                const timeSelect = [
                    {
                        label: '所有时段',
                        value: 'allTime',
                    },
                    {
                        label: '未来三天',
                        value: 'threeDayTime',
                    },
                    {
                        label: '未来一周',
                        value: 'oneWeekTime',
                    },
                    {
                        label: '未来一个月',
                        value: 'oneMouthTime',
                    },
                ];
                let seasons = [itemList, timeSelect];

                this.setState({ seasons });
            }
        });
    }

    render() {
    
        return (
            <div>
                <div className="at-top">
                    <div className="at-topcontent ">
                        <ul className="at-navtab clearfix" style={{ margin: '0' }}>
                            <li>
                                <a className="active">报名预约</a>
                            </li>
                            <li>
                                <a>往期回顾</a>
                            </li>
                        </ul>
                        <div className="at-search">
                            <Icon
                                type="search"
                                size={'md'}
                                onClick={() => {
                                    this.props.history.push('/search');
                                }}
                            />
                        </div>
                    </div>
                </div>

                <div className="n-l-header n-l-top110">
                    <div className="n-l-nav" style={{ margin: '0', height: '38px' }}>
                        <Flex style={{ width: '100%' }}>
                            <Flex.Item>
                                <Picker
                                    data={this.state.seasons}
                                    cascade={false}
                                    extra="所有分类,所有时段"
                                    value={this.state.sValue}
                                    onChange={(v) => this.setState({ sValue: v })}
                                    onOk={(sValue) => {
                                        this.changeSelect(sValue);
                                    }}>
                                    <selectList.Item arrow="horizontal">
                                        <button onClick={() => {}} className={this.state.selectButtonClassName}>
                                            <span>筛选</span>
                                        </button>
                                    </selectList.Item>
                                </Picker>
                            </Flex.Item>
                        </Flex>
                    </div>
                </div>
                <div style={{ marginTop: '96px' }}>
                    <List
                        ref={this.myList}
                        offset={this.state.offset}
                        limit={this.state.limit}
                        query={this.state.query}
                        maxSize={0}
                        {...this.props}
                    />
                </div>
                <ToBar {...this.props} />
            </div>
        );
    }

    changeSelect = (values: any) => {
        let selectButtonClassName = 'select-button';
        if (values[0] !== 'allType' || values[1] !== 'allTime') {
            selectButtonClassName = 'selected-button';
        }
        let query = this.selectParams({
            activityTime: values[1],
            activityType: values[0],
        });
        let offset = 0;
        let limit = 5;
        this.setState({ sValue: values, selectButtonClassName, query, offset, limit });
    };
    selectParams = (values: { activityTime: string; activityType: string; partyName?: string }) => {
        let query = 'isDel = 0 and approval = 1  and status = 1 ';
        let queryParam = '';
        if (!!values['activityTime'] && values['activityTime'] == 'threeDayTime') {
            let date = DateUtil.customizeDayTime(3);
            queryParam += ` and partyStartDate >= ${date[0]} and partyStartDate <= ${date[1]}     `;
        } else if (!!values['activityTime'] && values['activityTime'] == 'oneWeekTime') {
            let date = DateUtil.customizeDayTime(7);
            queryParam += ` and partyStartDate >= ${date[0]} and partyStartDate <= ${date[1]}    `;
        } else if (!!values['activityTime'] && values['activityTime'] == 'oneMouthTime') {
            let date = DateUtil.customizeDayTime(30);
            queryParam += ` and partyStartDate >= ${date[0]} and partyStartDate <= ${date[1]}    `;
        } else if (
            !!values['activityTime'] &&
            values['activityTime'] == 'allTime' &&
            values['activityType'] === 'alltype'
        ) {
            queryParam += ` and isCalendar = 0 `;
        }
        if (!!values['activityType'] && values['activityType'] !== 'allType') {
            queryParam += `and propertyId = "${values['activityType']}"`;
        }
        if (!!values['partyName']) {
            queryParam += `and partyName = ${values['partyName']}`;
        }
        return query + queryParam + ' sortby metadata.createdDate/sort.descending ';
    };
}

export default index;
