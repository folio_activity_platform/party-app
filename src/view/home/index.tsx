import React, { Component } from 'react';
import IProps from '../../components/IProps';
import IState from '../../components/IState';
import ToBar from '../../components/tabBar';
import List from '../../components/list';
import { fetchReaderReserve } from '../../api/index';
import DateUtil from '../../util/DateUtil';
class index extends Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            form: {},
            offset: 0,
            limit: 0,
            query: '',
            readerActivityList: [],
        };
    }
    componentWillMount() {
        sessionStorage.setItem('barKey', 'home');
    }
    componentDidMount() {
        this.getReaderActivity();
    }
    public toPath = (path: string) => {
        const {
            history: { push },
        } = this.props;
        push(path);
    };
    getReaderActivity = () => {
        let barcode = sessionStorage.getItem('barcode');
        if (barcode == null) {
            return false;
        }

        let query = ` isDel = 0 and readerReserveGroup == *"barcode"*: *"${barcode}"*  `;
        let str = DateUtil.freeObjFormat(new Date(), 'YYYY-MM-DD');

        query += `and partyEndDate >= ${str} 00:00:00  and partyStartDate <= ${str} 23:59:59   `;
        let params = {
            query,
            offset: 0,
            limit: 5,
        };

        fetchReaderReserve(params).then((response: any) => {
            let list = response.data.webReserveGroup;
            let tmp = [];

            if (list != null && list !== '' && list.length > 0) {
                for (let i = 0; i < list.length; i++) {
                    tmp.push(
                        <div
                            className="secModelHint"
                            style={{
                                whiteSpace: 'nowrap',
                                overflow: 'hidden',
                                textOverflow: 'ellipsis',
                            }}>
                            {list[i].partyName}
                        </div>
                    );
                }
            } else {
                tmp.push(<div className="secModelHint">暂无行程</div>);
            }

            this.setState({
                readerActivityList: tmp,
            });
        });
    };
    render() {
        return (
            <div style={{ backgroundColor: '#f5f6fa' }}>
                <div className="disFlex secondModel" style={{ padding: '.25rem .3rem 0', margin: 0 }}>
                    <a
                        className="secModelIn secModel01"
                        onClick={() => {
                            this.props.history.push('/reserve_all_today');
                        }}>
                        <span className="secModelTit">今日行程</span>
                        {this.state.readerActivityList}
                    </a>
                    <div className="secRight">
                        <a
                            className="secModelIn secModel02"
                            onClick={() => {
                                this.toPath('/user_content');
                                sessionStorage.setItem('barKey', 'content');
                            }}>
                            <span className="secModelTit">个人中心</span>
                        </a>
                        <div
                            className="disFlex secRtBot"
                            onClick={() => {
                                this.toPath('/calender');
                                sessionStorage.setItem('barKey', 'home');
                            }}>
                            <span className="secModelIn secModel03" style={{ width: '100%' }}>
                                <i className="secModelIcon"></i>
                                <span className="secModelTit">活动日历</span>
                                <p>活动早知道</p>
                            </span>
                        </div>
                    </div>
                </div>

                <div className="thirModel">
                    <div className="thirModelTit">
                        <div className="thirModelTitIn">推荐活动</div>
                        <span
                            className="thirModelMore"
                            onClick={() => {
                                this.props.history.push('/list');
                                sessionStorage.setItem('barKey', 'reserve');
                            }}>
                            更多
                        </span>
                    </div>
                </div>

                <List
                    offset={0}
                    limit={5}
                    query="(isDel = 0 and approval = 1  and status = 1 and isCalendar = 0) sortby metadata.createdDate/sort.descending "
                    maxSize={5}
                    {...this.props}
                />
                <ToBar {...this.props} />
            </div>
        );
    }
}

export default index;
