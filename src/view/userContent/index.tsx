import React, { Component } from 'react';
import IProps from '../../components/IProps';
import IState from '../../components/IState';
import ToBar from '../../components/tabBar';
import { fetchReaderReserve, fatchReserveQueue } from '../../api/index';
import DateUtil from '../../util/DateUtil';
interface State extends IState {
    all: number;
    wait: number;
    reserveQueue: number
}
class index extends Component<IProps, State> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            form: {},
            offset: 0,
            limit: 5,
            query: '',
            user: {},
            isLogin: false,
            all: 0,
            wait: 0,
            reserveQueue: 0
        };
    }

    // eslint-disable-next-line react/no-deprecated,@typescript-eslint/no-empty-function
    componentWillMount() {
        const barcode = sessionStorage.getItem('barcode');
        const userJSON = sessionStorage.getItem('user');
        if (!!barcode) {
            this.setState({ isLogin: true });
            this.getAll();
            this.getWait();
            this.getReserveQueue()
        } else {
            this.props.history.push('/login');
        }
        if(!!userJSON){
           const user=JSON.parse(userJSON);
           this.setState({
               user
           })
        }
    }
    componentDidMount(){

    }
    getAll = () => {
        let barcode = sessionStorage.getItem('barcode');
        if (barcode != null) {
            let query = ` isDel = 0 and readerReserveGroup == *"barcode"*: *"${barcode}"* `;
            const params = {
                offset: -1,
                limit: -1,
                query,
            };
            fetchReaderReserve(params)
                .then((response: any) => {
                    try {
                        const size = response.data.totalRecords;
                        this.setState({ all: size });
                    } catch (e) {
                        console.log(e);
                    }
                })
                .catch((e) => console.log(e));
        }
    };
    getWait = () => {
        let barcode = sessionStorage.getItem('barcode');
        if (barcode != null) {
            let query = ` isDel = 0 and readerReserveGroup == *"barcode"*: *"${barcode}"* `;
            let str = DateUtil.freeObjFormat(new Date(), 'YYYY-MM-DD');
            query += `and partyEndDate >= ${str} 00:00:00  and partyStartDate <= ${str} 23:59:59  && attendState = 5  `;
            const params = {
                offset: -1,
                limit: -1,
                query,
            };
            fetchReaderReserve(params)
                .then((response: any) => {
                    try {
                        const size = response.data.totalRecords;
                        this.setState({ wait: size });
                    } catch (e) {
                        console.log(e);
                    }
                })
                .catch((e) => console.log(e));
        }
    };
    getReserveQueue = () => {
        let barcode = sessionStorage.getItem('barcode');
        if (!!barcode) {
            fatchReserveQueue({ query: barcode }).then((response: any) => {
                const { data: { totalRecords } } = response
                this.setState({ reserveQueue: totalRecords })
            }).catch((e) => console.log(e));
        }
    }
    render() {
        return (
            <div>
                <div className={'userContent'}>
                    <div className="n-u-wrapper">
                        <div className="n-u-header"></div>
                        <div className="n-u-info">
                            <div
                                className="n-u-info-img"
                                onClick={() => {
                                    this.props.history.push('/user_info');
                                }}>
                                <a>
                                    <img src=" /images/user-img.png" />
                                </a>
                             
                            </div>
                            <p
                                className="n-u-info-name"
                                onClick={() => {
                                    this.props.history.push('/user_info');
                                }}>
                                <a style={{ color: '#333' }}>{this.state.user.username}</a>
                            </p>
                            <ul className="n-u-menu-list">
                                <li
                                    onClick={() => {
                                        this.props.history.push('/reserve_all_wait');
                                    }}>
                                    <a>
                                        <div className="n-u-menu-num">{this.state.wait}</div>
                                        <div className="n-u-menu-text">待参加</div>
                                    </a>
                                </li>
                                <li
                                    onClick={() => {
                                        this.props.history.push('/reserve_all_detail/all');
                                    }}>
                                    <a>
                                        <div className="n-u-menu-num">{this.state.all}</div>
                                        <div className="n-u-menu-text">我的活动</div>
                                    </a>
                                </li>
                                <li onClick={() => {
                                    this.props.history.push('/queue_list')
                                }}>
                                    <a>
                                        <div className="n-u-menu-num">{this.state.reserveQueue}</div>
                                        <div className="n-u-menu-text">我的排队</div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <ul className="n-u-template clearfix">
                            <li
                                onClick={() => {
                                    this.props.history.push('/user_info');
                                }}>
                                <a>
                                    <img src="/images/p6.png" />
                                    <div>设置</div>
                                </a>
                             
                             
                            </li>
                            <li
                                onClick={() => {
                                    this.props.history.push('/my_appraisal');
                                }}>
                                <a>
                                    <img src="/images/big21.png" />
                                    <div>我的评价</div>
                                </a>
                             
                             
                            </li>
                        </ul>

                        {this.state.isLogin ? (
                            <div
                                className="person-exit-btn"
                                onClick={() => {
                                    sessionStorage.removeItem('user');
                                    sessionStorage.removeItem('x-okapi-token');
                                    sessionStorage.removeItem('barcode');
                                    sessionStorage.setItem('barKey', 'home');
                                    this.props.history.push('/');
                                }}>
                                退出登录
                            </div>
                        ) : (
                                <div
                                    className="person-exit-btn"
                                    onClick={() => {
                                        sessionStorage.removeItem('user');
                                        sessionStorage.removeItem('x-okapi-token');
                                        sessionStorage.removeItem('barcode');
                                        sessionStorage.setItem('barKey', 'home');
                                        this.props.history.push('/login');
                                    }}>
                                    登录
                                </div>
                            )}
                    </div>
                </div>
                <ToBar {...this.props} />
            </div>
        );
    }
}

export default index;
