import React, { Component } from 'react';
import IProps from '../../components/IProps';
import IState from '../../components/IState';
import ToBar from '../../components/tabBar';

class index extends Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            form: {},
            offset: 0,
            limit: 5,
            query: '',
        };
    }

    componentWillMount() {}

    render() {
        return (
            <div>
                reserve
                <ToBar {...this.props} />
            </div>
        );
    }
}

export default index;
