import React, { Component } from 'react';
import { HashRouter as Router, Switch } from 'react-router-dom';
import routes from '../router';
import { RouteWithSubRoutes } from '../router/common';
import { RouteInterface } from '../router/interface';

export interface Props {
    name?: string;
    enthusiasmLevel?: number;
    className?: string;
    class?: string;
}
interface State {
    name?: string;
    enthusiasmLevel?: number;
}
class index extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { name: '张三' };
    }

    render() {
        return (
            <Router>
                <Switch>
                    {routes.map((route: RouteInterface, i: number) => {
                        return RouteWithSubRoutes(route, i);
                    })}
                </Switch>
            </Router>
        );
    }
}

export default index;
