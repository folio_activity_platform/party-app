import React, { Component } from 'react';
import IProps from '../../components/IProps';
import IState from '../../components/IState';
import { Formik } from 'formik';
import { Toast } from 'antd-mobile';
import { loginFunc } from '../../util/login';
class index extends Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            form: {},
            offset: 0,
            limit: 5,
            query: '',
        };
    }

    render() {
        const { history } = this.props;
        return (
            <div>
                <div className="quick-top">
                    <div className="login-text">
                        <h2>欢迎登录</h2>
                    </div>
                    <Formik
                        initialValues={{ username: '', password: '' }}
                        onSubmit={(values, actions) => {
                            Toast.loading('正在登录...', 20);
                            loginFunc(values)
                                .then((response: any) => {
                                    Toast.hide();
                                    history.push('/');
                                })
                                .catch((e) => Toast.fail(e.message));
                        }}
                        render={(p) => (
                            <form onSubmit={p.handleSubmit}>
                                <div className="login-line">
                                    <input
                                        className="id-phone"
                                        type="text"
                                        onChange={p.handleChange}
                                        onBlur={p.handleBlur}
                                        value={p.values.username}
                                        name="username"
                                        placeholder="请输入姓名"
                                        required
                                    />
                                </div>
                                <div className="login-line">
                                    <input
                                        className="id-phone"
                                        type="password"
                                        onChange={p.handleChange}
                                        onBlur={p.handleBlur}
                                        name="password"
                                        placeholder="请输入密码"
                                        required
                                    />
                                </div>
                                <input type="submit" className="weixin-btn" value="登录" style={{ width: '100%' }} />
                                <input
                                    type="button"
                                    className="weixin-btn-goback"
                                    value="返回"
                                    style={{ width: '100%' }}
                                    onClick={() => {
                                        history.push('/');
                                    }}
                                />
                            </form>
                        )}></Formik>
                </div>
            </div>
        );
    }
}

export default index;
