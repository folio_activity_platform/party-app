import React from 'react';

import Index from './view/index';
import ToBar from './components/tabBar';
import './App.css';
import './css/home/base.css';
import './css/home/index.css';
import './css/calendar.css';
import './css/activityCalendar.css';
import './css/activitytrain.css';
import './css/color.css';
import './css/face.css';
import './css/gzfy.css';
import './css/gzfy20190527.css';
import './css/iconfont.css';
import './css/index.css';
import './css/login.css';
import './css/only-index.css';
import './css/otherTwo.css';
import './css/person.css';
import './css/qiandao.css';
import './css/toupiao.css';

import './css/public/base.css';
import './css/public/dropload.css';
import './css/public/iosSelect.css';
import './css/public/nav.css';

import './css/main/index.css';
import './css/data/index.css';
class App extends React.Component {
    componentDidMount() {
        const docEl = document.documentElement,
            design = 750;
        var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';
        var recale = function() {
            var clientWidth = docEl.clientWidth;
            if (!clientWidth) return;
            docEl.style.fontSize = 100 * (clientWidth / design) + 'px';
        };
        if (!document.addEventListener) return;
        window.addEventListener(resizeEvt, recale, false);
        docEl.addEventListener('DOMContentLoaded', recale, false);
        recale();
    }
    render() {
        return <Index className="App"></Index>;
    }
}

export default App;
