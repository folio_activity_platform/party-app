import Home from '../view/home';
import Reserve from '../view/reserve';
import List from '../view/partyList';
import Search from '../view/partyList/search';
import UserContent from '../view/userContent';
import Login from '../view/login';
import { All, Today, Wait, Setting } from '../components/usercontent';
import { Details, Reserves, ReserveResult,ReservesQueue } from '../components/partydetails';
import UserSet from '../view/user_setting';
import Calender from '../view/calender';
import {AppraisalForm,AppraisalDetail} from '../components/appraisal'
import QueueList from '../view/queuelist/index'
import Appraisal from '../view/appraisal/index'
 
const routes = [
    {
        path: '/',
        component: Home,
    },
    {
        path: '/reserve',
        component: Reserve,
    },
    {
        path: '/list',
        component: List,
    },
    {
        path: '/search',
        component: Search,
    },
    {
        path: '/user_content',
        component: UserContent,
    },
    {
        path: '/login',
        component: Login,
    },
    {
        path: '/reserve_all_detail/:id',
        component: All,
    },
    {
        path: '/reserve_all_today',
        component: Today,
    },
    {
        path: '/reserve_all_wait',
        component: Wait,
    },
    {
        path: '/user_setting',
        component: Setting,
    },
    {
        path: '/party_details/:id',
        component: Details,
    },
    {
        path: '/party_reserve/:id',
        component: Reserves,
    },
    {
        path: '/party_reserve_queue/:id',
        component: ReservesQueue,
    },
    {
        path: '/reserve_result/:id',
        component: ReserveResult,
    },
    {
        path: '/user_info',
        component: UserSet,
    },
    {
        path: '/calender',
        component: Calender,
    },
    {
        path: '/appraisal_form/:partyId/:reserveId',
        component: AppraisalForm
    },{
        path: '/queue_list',
        component: QueueList
    },{
        path: '/my_appraisal',
        component: Appraisal
    },{
        path: '/appraisal_detail/:id',
        component:AppraisalDetail
    }
];
export default routes;
