interface routeInterface {
    path: string;
    component: any;
}

export type RouteInterface = routeInterface;
