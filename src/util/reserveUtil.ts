import moment from 'moment';

function partyTimeComparison(party: any) {
    let partyStartDate = party.partyStartDate;
    let partyEndDate = party.partyEndDate;
    let attendStartDate = party.attendStartDate;
    let attendEndDate = party.attendEndDate;
    let reserveStartDate = party.reserveStartDate;
    let reserveEndDate = party.reserveEndDate;
    const newDate = moment();
    let result: object = {
        description: '',
        code: 0,
        status: true,
    };
    if (!!!attendStartDate && !!!attendEndDate && !!!reserveStartDate && !!!reserveEndDate) {
        if (newDate.isBefore(partyStartDate)) {
            result = {
                code: 1,
                status: false,
            };
            return result;
        } else if (newDate.isAfter(partyEndDate)) {
            result = {
                // description: '已结束',
                code: 4,
                status: false,
            };
            return result;
        } else {
            result = {
                //description: '活动无需报名,直接参与',
                code: 3,
                status: false,
            };
            return result;
        }
    }

    if (newDate.isBefore(party.reserveStartDate)) {
        result = {
            // description: '未开始',
            code: 1,
            status: false,
        };
        return result;
    }
    const s = new Set();
    [partyStartDate, partyEndDate, attendStartDate, attendEndDate, reserveStartDate, reserveEndDate].forEach((x) =>
        s.add(x)
    );
    let size = s.size;
    let isTimeEqualsResult = false;
    if (size === 1) {
        isTimeEqualsResult = true;
    }
    if (newDate.isAfter(party.partyEndDate)) {
        result = {
            // description: '已结束',
            code: 4,
            status: false,
        };
        return result;
    } else {
        if (isTimeEqualsResult) {
            if (newDate.isBetween(newDate, partyStartDate, partyEndDate)) {
                result = {
                    //description: '未开始',
                    code: 2,
                    status: false,
                };
                return result;
            } else if (newDate.isAfter(party.partyStartDate)) {
                result = {
                    // description: '已结束',
                    code: 4,
                    status: false,
                };
                return result;
            }
        } else {
            if (moment().isBetween(newDate, reserveStartDate, reserveEndDate)) {
                result = {
                    //description: '未开始',
                    code: 2,
                    status: false,
                };
                return result;
            } else if (moment().isBetween(newDate, attendStartDate, attendEndDate)) {
                result = {
                    //description: '未开始',
                    code: 3,
                    status: false,
                };
                return result;
            } else if (moment().isBetween(newDate, partyStartDate, partyEndDate)) {
                result = {
                    //description: '未开始',
                    code: 3,
                    status: false,
                };
                return result;
            } else {
                result = {
                    //description: '未开始',
                    code: 1,
                    status: false,
                };
                return result;
            }
        }
    }
}
export { partyTimeComparison };
