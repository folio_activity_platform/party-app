import { getImage ,getImageById} from '../api/index';
export function setImage(item: any, index: number) {
    return new Promise((resolve, reject) => {
        if (!item['image'] && !!item.imageId) {
            getImageById (item.imageId).then((res: any) => {
                let image = res.data.content;
                if(image){
                    resolve(image);
                }else{
                    resolve('');
                }
            });
        }
    });
}
export function setImageById(id: any) {
    return new Promise((resolve, reject) => {
        if (!!id) {
            getImageById(id).then((res: any) => {
                let image = res.data.content;
                if(image){
                    resolve(image);
                }else{
                    resolve('');
                }
            })
            .catch((e) => {
                resolve('');
            });
        } else {
            resolve('');
        }
    });
}
 
