import moment from 'moment';
moment.locale('zh-cn');
export default class DateUtil {
    static weekday: string[] = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'];
    /**
     * 基本的格式化 将时间字符串格式化为 YYYY-MM-DD HH:mm
     * @param time
     */
    public static baseFormat(time: string): string {
        if (!time) {
            return '';
        }
        return this.freeFormat(time, 'YYYY-MM-DD HH:mm');
    }
    public static freeFormat(time: string, format: string): string {
        if (!time || !format) {
            return '';
        }
        return moment(new Date(time.replace(/-/g, '/'))).format(format);
    }
    public static freeObjFormat(time: any, format: string): string {
        if (!time || !format) {
            return '';
        }
        return moment(time).format(format);
    }
    public static dateFormat(time: string): string {
        if (!time) {
            return '';
        }
        return this.freeFormat(time, 'YYYY-MM-DD');
    }
    public static timeFormat(time: string): string {
        if (!time) {
            return '';
        }
        return this.freeFormat(time, 'HH:mm');
    }

    /**
     * 判断日期是否相等 如果相等就格式化成 YYYY-MM-DD 周一 HH：MM
     * @param startTime
     * @param endTime
     */
    public static mergeFormat(startTime: string, endTime: string): string {
        if (!startTime || !endTime) {
            return '';
        }
        try {
            const start = this.dateFormat(startTime);
            const end = this.dateFormat(endTime);
            if (start === end) {
                const time = this.timeFormat(startTime);
                const day = this.freeFormat(start, 'E');
                return `${start} ${this.weekday[parseInt(day)]} ${time}`;
            }
            return `${this.baseFormat(startTime)} ~ ${this.baseFormat(endTime)}`;
        } catch (e) {
            console.error(e);
            return `${startTime} ~ ${endTime}`;
        }
    }
    public static customizeDayTime(amount: number) {
        let dates = [];
        let date = moment().format('YYYY-MM-DD HH:mm:ss');
        let afterDate = moment()
            .add(amount, 'day')
            .format('YYYY-MM-DD HH:mm:ss');
        dates = [date, afterDate];
        return dates;
    }
    public static paramReplace(source: string, target: string) {
        if (!!source) {
            return source.replace(/%{([\s\S]+?)}/g, target);
        }
        return null;
    }
}

// // eslint-disable-next-line no-extend-native

// var DateUtil = {
//   // YYYY-MM-DD hh:mm
//   format: function(start, end) {
//         return moment(new Date((start + ' ' + end).replace(/-/g, '/'))).format('YYYY-MM-DD HH:mm');

//   },
//   formats: function(time) {
//     return moment(new Date(time.replace(/-/g, "/"))).format("YYYY-MM-DD HH:mm");
//   },
//   formatsYYYYMMDD: function() {
//     return moment(new Date()).format("YYYY-MM-DD");
//   },
//   formatByDate(dateTime, formatStr) {
//     return moment(dateTime).format(formatStr);
//   },
//   formatByStr(start, end, formatStr) {
//     //console.log(start + " " + end);
//     return moment(new Date((start + " " + end).replace(/-/g, "/"))).format(
//       formatStr
//     );
//   },
//   formatByApple(start, formatStr) {
//     //console.log(start + " " + end);
//     return moment(new Date(start.replace(/-/g, "/"))).format(formatStr);
//   },
//   getWeek(date) {
//     // 参数时间戳
//     let week = moment(date).day();
//     switch (week) {
//       case 1:
//         return "周一";
//       case 2:
//         return "周二";
//       case 3:
//         return "周三";
//       case 4:
//         return "周四";
//       case 5:
//         return "周五";
//       case 6:
//         return "星期六";
//       case 0:
//         return "星期日";
//     }
//   },
//   formatDayTime: function(startDate, startTime, endDate, endTime) {
//     try {
//       if (!!startDate && !!endDate) {
//         let result = "";
//         if (startDate !== endDate) {
//           result =
//             this.format(startDate, startTime) +
//             " - " +
//             this.format(endDate, endTime);
//           return result;
//         }
//         result =
//           startDate +
//           " " +
//           this.formatByStr(startDate, startTime, "HH:mm") +
//           " ~ " +
//           this.formatByStr(endDate, endTime, "HH:mm") +
//           " " +
//           this.getWeek(new Date(startDate));
//         return result;
//       }
//     } catch (error) {
//       //console.log(error);
//     }
//   },
//   formatTowDayTime: function(startDate, endDate) {
//     try {
//       if (!!startDate && !!endDate) {
//         let result = "";
//         if (startDate !== endDate) {
//           result = this.formats(startDate) + " - " + this.formats(endDate);
//           return result;
//         }
//         result =
//           startDate +
//           " " +
//           this.formatByApple(startDate, "HH:mm") +
//           " ~ " +
//           this.formatByApple(endDate, "HH:mm") +
//           " " +
//           this.getWeek(new Date(startDate));
//         return result;
//       }
//     } catch (error) {
//       //console.log(error);
//     }
//   }
// };
// export default DateUtil;
