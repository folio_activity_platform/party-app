import moment from 'moment';
import moments from 'moment-timezone';

function smartStatus(response: any) {
    const date = moment(response.createDate); //当前时间节点
    const readerIsReserve = response.readerIsReserve;
    const reserveAmount = response.reserveAmount;
    const party = response.party;
    const category = party.category;
    const quota = party.quota;

    let status = {
        code: 1,
        status: false,
        description: '',
    };
    if (readerIsReserve) {
        let result: any = createStatus('已报名', party, date);
        result.status = false;
        return result;
    }
    // if (reserveAmount >= quota) {
    //     let result: any = createStatus('活动名额已满', party, date);
    //     result.status = false;
    //     return result;
    // }
    return createStatus(null, party, date);
}

function createStatus(description: string | null, party: any, date: any) {
    if (!!!date) {
        date = moment();
    }
    let name = getExploreName();
    let nameFlag = 'Safari' === name ? true : false;
    let dateStr = date.format('YYYY/MM/DD HH:mm:ss');
    let dateObject = date.toDate();
    let result = {
        code: 1,
        status: false,
        description: '',
    };

    let partyStartDate = moment(party.partyStartDate).format('YYYY/MM/DD HH:mm:ss');
    let partyEndDate = moment(party.partyEndDate).format('YYYY/MM/DD HH:mm:ss');
    let attendStartDate = '';
    let attendEndDate = '';
    let reserveStartDate = '';
    let reserveEndDate = '';
    if (party.category !== 3) {
        attendStartDate = moment(party.attendStartDate).format('YYYY/MM/DD HH:mm:ss');
        attendEndDate = moment(party.attendEndDate).format('YYYY/MM/DD HH:mm:ss');
        reserveStartDate = moment(party.reserveStartDate).format('YYYY/MM/DD HH:mm:ss');
        reserveEndDate = moment(party.reserveEndDate).format('YYYY/MM/DD HH:mm:ss');
    }
    //console.log((moment(dateStr).isBetween(partyStartDate,partyEndDate)))
    //  不用报名
    if (party.category === 3) {
        if (date.isBefore(partyStartDate)) {
            result = {
                description: '未开始',
                code: 1,
                status: false,
            };
            return result;
        } else if (date.isAfter(partyEndDate)) {
            result = {
                description: '已结束',
                code: 4,
                status: false,
            };
            return result;
        } else if (moment(dateStr).isBetween(partyStartDate, partyEndDate)) {
            result = {
                description: '活动无需报名,直接参与',
                code: 3,
                status: false,
            };
            return result;
        }
    } else {
        //需要报名分支
        if (date.isBefore(reserveStartDate)) {
            result = {
                description: '未开始',
                code: 1,
                status: false,
            };
            return result;
        }
        if (date.isAfter(partyEndDate)) {
            result = {
                description: '已结束',
                code: 4,
                status: false,
            };
            return result;
        }
        const s = new Set();
        [partyStartDate, partyEndDate, attendStartDate, attendEndDate, reserveStartDate, reserveEndDate].forEach((x) =>
            s.add(x)
        );
        let size = s.size;
        let isTimeEqualsResult = false;
        if (size === 2) {
            isTimeEqualsResult = true;
        }
        if (isTimeEqualsResult) {
            if (moment(dateStr).isBetween(partyStartDate, partyEndDate)) {
                result = {
                    description: description || '去报名',
                    code: 2,
                    status: true,
                };
                return result;
            } else if (date.isAfter(partyStartDate)) {
                result = {
                    description: '已结束',
                    code: 4,
                    status: false,
                };
                return result;
            }
        } else {
            if (moment(dateStr).isBetween(reserveStartDate, reserveEndDate)) {
                result = {
                    description: description || '去报名',
                    code: 2,
                    status: true,
                };
                return result;
            } else if (moment(dateStr).isBetween(attendStartDate, attendEndDate)) {
                result = {
                    description: '签到中',
                    code: 3,
                    status: false,
                };
                return result;
            } else if (moment(dateStr).isBetween(partyStartDate, partyEndDate)) {
                result = {
                    description: '进行中',
                    code: 3,
                    status: false,
                };
                return result;
            } else {
                result = {
                    description: '未开始',
                    code: 1,
                    status: false,
                };
                if (date.isAfter(reserveEndDate) && date.isBefore(attendStartDate)) {
                    result.description = '报名已截止';
                }
                return result;
            }
        }
    }
}
function getExploreName() {
    let userAgent = navigator.userAgent;
    if (userAgent.indexOf('Opera') > -1 || userAgent.indexOf('OPR') > -1) {
        return 'Opera';
    } else if (userAgent.indexOf('compatible') > -1 && userAgent.indexOf('MSIE') > -1) {
        return 'IE';
    } else if (userAgent.indexOf('Edge') > -1) {
        return 'Edge';
    } else if (userAgent.indexOf('Firefox') > -1) {
        return 'Firefox';
    } else if (userAgent.indexOf('Safari') > -1 && userAgent.indexOf('Chrome') == -1) {
        return 'Safari';
    } else if (userAgent.indexOf('Chrome') > -1 && userAgent.indexOf('Safari') > -1) {
        return 'Chrome';
    } else if ('ActiveXObject' in window) {
        return 'IE>=11';
    } else {
        return 'Unkonwn';
    }
}
function defaultParser(value: any, timeZone: string, dateFormat: string) {
    if (!value || value === '') {
        return value;
    }

    const offsetRegex = /T[\d.:]+[+-][\d]+$/;
    let inputMoment;
    // if date string contains a utc offset, we can parse it as utc time and convert it to selected timezone.
    if (offsetRegex.test(value)) {
        inputMoment = moments.tz(value, timeZone);
    } else {
        inputMoment = moments.tz(value, dateFormat, timeZone);
    }
    const inputValue = inputMoment.format(dateFormat);
    return inputValue;
}
export { smartStatus, createStatus };
