import { login } from '../api/index';
import { Modal, Toast } from 'antd-mobile';
import loginResponse from '../pojo/loginResponse';
const prompt = Modal.prompt;
function loginFunc(values: { username: string; password: string }) {
    return new Promise((reply, err) => {
        login(values)
            .then(function(response: any) {
                const res = response;
                if (res['headers']) {
                    const headers = res['headers'];
                    const xOkapiToken = headers['x-okapi-token'];
                    sessionStorage.setItem('x-okapi-token', xOkapiToken);
                }
                if (res['data']) {
                    const user = res['data'].user;
                    const barcode = user.barcode;

                    sessionStorage.setItem('user', JSON.stringify(user));
                    sessionStorage.setItem('barcode', barcode);
                }
                reply(new loginResponse(true, 'success', response, '2'));
                sessionStorage.setItem('barKey', 'home');
            })
            .catch((e) => {
                err(new loginResponse(false, e, e, '1'));
            });
    });
}
function loginModel() {
    return new Promise((reply, reject) => {
        return prompt(
            '登录',
            '',
            [
                {
                    text: '取消',
                    onPress: (value: any) => {
                        reject(new loginResponse(false, '用户取消', '用户取消', '0'));
                        return;
                    },
                },
                {
                    text: '确定',
                    onPress: function(username: string, password: string) {
                        const values = {
                            username,
                            password,
                        };
                        if (!!!username) {
                            reject(new loginResponse(false, '请输入登录名称 ', '请输入登录名称', '1'));
                            return;
                        }
                        if (!!!password) {
                            reject(new loginResponse(false, '请输入登录密码', '请输入登录密码', '1'));
                            return;
                        }
                        loginFunc(values)
                            .then((response: any) => {
                                if (!!response) {
                                    reply(new loginResponse(false, '登录成功！', '登录成功！', '2'));
                                }
                            })
                            .catch((e: any) => {
                                reject(e);
                                return;
                            });
                    },
                },
            ],
            'login-password',
            '',
            ['请输入您的登录名称', '请输入您的登录密码']
        );
    });
}
function loginNext(then: any, callback: Function) {
    loginModel()
        .then((response) => {
            callback();
        })
        .catch((e) => {
            if (e instanceof loginResponse) {
                switch (e.action) {
                    case '0':
                        then.props.history.push('/');
                        break;
                    case '1':
                        Toast.fail('出错了！' + e.message, 2, () => {
                            loginNext(then, callback);
                        });
                        break;
                    default:
                        then.props.history.push('/');
                        break;
                }
            } else {
                console.info(e);
                Toast.fail('出错了！' + e.message, 2, () => {
                    then.props.history.push('/');
                });
            }
        });
}
export { loginFunc, loginModel, loginNext };
